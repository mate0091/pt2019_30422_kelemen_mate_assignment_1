package Algorithms;

import java.util.ArrayList;

public class Polynomial
{
    private ArrayList<Monomial> terms;
    private int degree;

    public Polynomial(int degree)
    {
        this.degree = degree;

        terms = new ArrayList<>();

        for(int i = 0; i <= degree; i++)
        {
            terms.add(new Monomial(0, i));
        }
    }

    public Polynomial(ArrayList<Monomial> terms)
    {
        this.terms = terms;
        this.degree = 0;
        this.fixDegree();
    }

    public Polynomial(Monomial m)
    {
        this.terms = new ArrayList<>();
        this.degree = m.getDegree();

        for(int i = 0; i <= this.degree; i++)
        {
            terms.add(i, new Monomial(0, i));
        }

        this.terms.set(m.getDegree(), m);
    }

    public int getDegree()
    {
        return this.degree;
    }

    public Polynomial add(Polynomial other)
    {
        int newDegree = Math.max(this.degree, other.getDegree());

        Polynomial q = new Polynomial(newDegree);

        this.extend(newDegree);
        other.extend(newDegree);

        for(int i = 0; i <= newDegree; i++)
        {
            Monomial current;
            Monomial otherMonomial;

            current = terms.get(i);
            otherMonomial = other.getTerms().get(i);

            current = current.add(otherMonomial);

            q.getTerms().set(current.getDegree(), current);
        }

        this.fixDegree();
        other.fixDegree();

        return q;
    }

    public Polynomial sub(Polynomial other)
    {
        int newDegree = Math.max(this.degree, other.getDegree());

        Polynomial q = new Polynomial(newDegree);

        //extend these polys
        this.extend(newDegree);
        other.extend(newDegree);

        for(int i = 0; i <= newDegree; i++)
        {
            Monomial current;
            Monomial otherMonomial;

            current = terms.get(i);
            otherMonomial = other.getTerms().get(i);

            current = current.sub(otherMonomial);

            q.getTerms().set(current.getDegree(), current);
        }

        //fix degree of polys
        this.fixDegree();
        other.fixDegree();

        return q;
    }

    public Polynomial mult(Polynomial other)
    {
        int newDegree = degree + other.getDegree();

        Polynomial q = new Polynomial(newDegree);

        for (int i = 0; i <= this.degree; i++)
        {
            for(int j = 0; j <= other.getDegree(); j++)
            {
                //put at i+jth position the computed term and add to itself
                Monomial current = q.getTerms().get(i + j);
                Monomial A = terms.get(i);
                Monomial B = other.getTerms().get(j);
                current = current.add(A.mult(B));

                q.getTerms().set(i + j, current);
            }
        }

        q.fixDegree();

        return q;
    }

    public Polynomial[] div(Polynomial other)
    {
        Polynomial q = new Polynomial(this.degree);
        Polynomial r = new Polynomial(this.terms);

        try {

            while (!r.isEmpty() && r.getDegree() >= other.getDegree()) {
                Monomial ratio = r.lead().div(other.lead());

                if (ratio.getCoefficient() == 0) break;

                Polynomial t = new Polynomial(ratio);
                q = q.add(t);
                r = r.sub(t.mult(other));
            }
        }

        catch (ArithmeticException e) //catch division by 0
        {
            q = new Polynomial(0);
            r = new Polynomial(0);
        }

        q.fixDegree();
        r.fixDegree();

        Polynomial[] result = {q, r};

        return result;
    }

    public Polynomial differentiate()
    {
        Polynomial q = new Polynomial(degree - 1);

        for(int i = 1; i <= degree; i++)
        {
            Monomial current = terms.get(i);
            current = current.differentiate();
            q.getTerms().set(i - 1, current);
        }

        q.fixDegree();

        return q;
    }

    public Polynomial integrate()
    {
        Polynomial q = new Polynomial(degree + 1);

        for(int i = 0; i <= degree; i++)
        {
            Monomial current = terms.get(i);
            current = current.integrate();
            q.getTerms().set(i + 1, current);
        }

        return q;
    }

    private Monomial lead()
    {
        return this.terms.get(this.degree);
    }

    public ArrayList<Monomial> getTerms()
    {
        return this.terms;
    }

    public String toString()
    {
        String result = "";

        for(Monomial i : terms)
        {
            result = result.concat(i.toString());
        }

        return result;
    }

    private boolean isEmpty()
    {
        this.fixDegree();

        for(Monomial m : this.terms)
        {
            if(m.getCoefficient() != 0)
            {
                return false;
            }
        }

        return true;
    }

    private void fixDegree()
    {
        int newDegree = this.terms.size() - 1;

        for(int i = newDegree; i >= 0 ; i--)
        {
            Monomial current = terms.get(i);

            if(current.getCoefficient() == 0)
            {
                //delete current, decrement newdegree
                terms.remove(i);
                newDegree--;
            }

            else break;
        }

        this.degree = newDegree;
    }

    private void extend(int newDegree)
    {
        if(newDegree - this.degree > 0)
        {
            for(int i = this.degree + 1; i <= newDegree; i++) this.terms.add(i, new Monomial(0, i));
        }

        this.degree = newDegree;
    }
}
