package Algorithms;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Monomial implements Comparable<Monomial>
{
    private float coefficient;
    private int degree;

    public Monomial(float coefficient, int degree)
    {
        this.coefficient = coefficient;
        this.degree = degree;
    }

    public float getCoefficient() {
        return coefficient;
    }

    public int getDegree() {
        return degree;
    }

    public Monomial add(Monomial other) throws ArithmeticException
    {
        if(compareTo(other) != 0)
        {
            throw new ArithmeticException("Degrees are not equal!");
        }

        return new Monomial(this.coefficient + other.getCoefficient(), this.degree);
    }

    public Monomial sub(Monomial other) throws ArithmeticException
    {
        if(compareTo(other) != 0)
        {
            throw new ArithmeticException("Degrees are not equal!");
        }

        return new Monomial(this.coefficient - other.getCoefficient(), this.degree);
    }

    public Monomial mult(Monomial other)
    {
        return new Monomial(this.coefficient * other.getCoefficient(), this.degree + other.getDegree());
    }

    public Monomial div(Monomial other) throws NotImplementedException
    {
        return new Monomial((int)(this.coefficient / other.getCoefficient()), degree - other.getDegree());
    }

    public Monomial differentiate()
    {
        return new Monomial(this.coefficient * this.degree, this.degree - 1);
    }

    public Monomial integrate()
    {
        return new Monomial(this.coefficient / (this.degree + 1), this.degree + 1);
    }

    public String toString()
    {
        String result = "";

        if(coefficient == 0) return result;

        if(this.coefficient > 0)
        {
            result = result.concat("+");
        }

        result = result.concat(Float.toString(this.coefficient));

        if(this.degree > 0)
        {
            result = result.concat("x");

            if(this.degree > 1) result = result.concat("^" + this.degree);
        }

        return result;
    }

    public int compareTo(Monomial o) {
        return this.degree - o.getDegree();
    }
}
