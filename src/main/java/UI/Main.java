package UI;

public class Main
{
    PolyModel model;
    PolyView view;
    PolyController controller;

    public Main()
    {
        model = new PolyModel();
        view = new PolyView(model);
        controller = new PolyController(view, model);
    }

    public static void main(String[] args)
    {
        new Main();
    }
}
