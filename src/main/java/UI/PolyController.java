package UI;

public class PolyController
{
    private PolyModel model;
    private PolyView view;

    public PolyController(PolyView view, PolyModel model)
    {
        this.model = model;
        this.view = view;

        this.view.addOperationListener(e ->
        {
            String[] poly = this.view.getPoly();
            String currentOperation = this.view.getSelection();
            boolean flag = false;
            /*tell model to store the two polynomials, parse them
            and compute the result according to the currently selected
            operation
            */

            try {
                this.model.setPolys(poly);
                this.model.computeOperation(currentOperation);
            }
            catch (Exception e1)
            {
                //e1.printStackTrace();
                flag = true;
            }

            if(flag)  this.view.setOutput("Invalid input");
            else this.view.setOutput(this.model.getResult());
        });
    }
}
