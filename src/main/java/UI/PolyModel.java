package UI;

import Algorithms.Monomial;
import Algorithms.Polynomial;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class PolyModel
{
    Polynomial p, q;

    String result;

    public PolyModel()
    {
        p = new Polynomial(0);
        q = new Polynomial(0);
    }

    private Polynomial setPoly(@NotNull String poly)
    {
        ArrayList<Monomial> pTerms = new ArrayList<Monomial>();
        int degree = 0;

        //tokenize
        String p1 = poly.replace("-", "+-").trim();
        String[] arr = p1.split("\\+");

        for(String i : arr)
        {
            if(i.equals("")) continue; //skip if empty

            String[] tempMonomial = i.split("x\\^", 2); //split by ^
            Monomial current;

            if(tempMonomial.length == 1) //process single element format
            {
                if(tempMonomial[0].contains("x"))// if term with degree 1
                {
                    tempMonomial[0] = tempMonomial[0].replace("x","");
                    current = new Monomial(Integer.parseInt(tempMonomial[0]),1);
                }

                else current = new Monomial(Integer.parseInt(tempMonomial[0]), 0);
            }

            else current = new Monomial(Integer.parseInt(tempMonomial[0]), Integer.parseInt(tempMonomial[1]));

            if(current.getDegree() > degree) degree = current.getDegree();

            pTerms.add(current);
        }

        Polynomial t = new Polynomial(degree);

        for(Monomial m : pTerms)
        {
            t.getTerms().set(m.getDegree(), m);
        }

        return t;
    }

    public void setPolys(String[] polys)
    {
        p = setPoly(polys[0]);
        q = setPoly(polys[1]);

        System.out.println("p: " + p);
        System.out.println("q: " + q);
    }

    public void computeOperation(String operation)
    {
        if (operation.equalsIgnoreCase("Addition"))
        {
            result = p.add(q).toString();
        }

        else if (operation.equalsIgnoreCase("Subtraction"))
        {
            result = p.sub(q).toString();
        }

        else if (operation.equalsIgnoreCase("Multiplication"))
        {
            result = p.mult(q).toString();
        }

        else if (operation.equalsIgnoreCase("Division"))
        {
            result = "q: " + p.div(q)[0].toString() + "\nr: " + p.div(q)[1].toString();
        }

        else if (operation.equalsIgnoreCase("Integration"))
        {
            result = p.integrate().toString();
        }

        else if (operation.equalsIgnoreCase("Differentiation"))
        {
            result = p.differentiate().toString();
        }
    }

    public String getResult()
    {
        return this.result;
    }
}
