package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class PolyView extends JFrame
{
    private JTextArea inputPoly1, inputPoly2;
    private JTextArea outputPoly;
    private JPanel panel;

    private JButton calculateBtn;
    private JComboBox<String> comboBox;
    private String[] opTexts = {"Addition", "Subtraction", "Multiplication", "Division", "Differentiation", "Integration"};

    public PolyView(PolyModel model)
    {
        initUI();
    }

    private void initUI()
    {
        inputPoly1 = new JTextArea("Polynomial 1", 3, 15);
        inputPoly2 = new JTextArea("Polynomial 2", 3, 15);
        outputPoly = new JTextArea("Output polynomial", 3, 15);
        outputPoly.setEditable(false);
        inputPoly1.setEditable(true);
        inputPoly2.setEditable(true);

        comboBox = new JComboBox<String>(opTexts);
        calculateBtn = new JButton("Calculate");

        panel = new JPanel();
        panel.setLayout(new FlowLayout());

        panel.add(new JLabel("P:"));
        panel.add(inputPoly1);

        panel.add(new JLabel("Q:"));
        panel.add(inputPoly2);

        panel.add(comboBox);
        panel.add(new JLabel("Result:"));
        panel.add(outputPoly);

        panel.add(calculateBtn);

        setContentPane(panel);
        pack();
        setTitle("Polynomial processing app");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(new Dimension(500, 200));
        setResizable(false);
    }

    public String[] getPoly()
    {
        String[] poly = {inputPoly1.getText(), inputPoly2.getText()};

        return poly;
    }

    public void setOutput(String q)
    {
        this.outputPoly.setText(q);
    }

    public void addOperationListener(ActionListener opListener)
    {
        this.calculateBtn.addActionListener(opListener);
    }

    public String getSelection()
    {
        return String.valueOf(this.comboBox.getSelectedItem());
    }
}
